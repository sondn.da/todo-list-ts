const check = (key: string) => {
  if(window.localStorage.getItem(key)){
    try{
      let data: string | null = window.localStorage.getItem(key);
      if(data === null){
        data = "";
      }
      return JSON.parse(data);
    }catch{
      window.localStorage.removeItem(key);
    }
  }
  return null;
}

const save = (key: string, value: Array<any> | string) => {
  window.localStorage.setItem(key, JSON.stringify(value))
}

export default {
  check,
  save
}