import Vue from "vue";
import Vuex from "vuex";
import TaskModule from './module/task';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    TaskModule: TaskModule,
  }
});
