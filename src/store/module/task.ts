import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { Task } from '../../models/task';
import localStorageService from '../../api/localStorage.service';
import store from '../index'

const TASKS_KEY = "tasks";

@Module({namespaced: true, store })
class TaskModule extends VuexModule{
  public tasks: Array<Task> = [];
  
  get allTasks(): Array<Task>{
    return this.tasks;
  }

  @Mutation
  public setState(data: Array<Task>){
    this.tasks = data;
  }
  @Mutation
  public add(task: Task){
    this.tasks.push(task)
  }
  @Mutation
  public update(task: Task){
    const idx = this.tasks.findIndex(t => t.id === task.id);
    this.tasks.splice(idx, 1, {...task})
  }
  @Mutation
  public delete(id: number){
    const newTasks = this.tasks.filter(task => task.id !== id)
    this.tasks = newTasks;
  }
  @Mutation
  public bulk(ids: Array<number>){
    const newTasks = this.tasks.filter(task => {
      return !ids.includes(task.id);
    })
    this.tasks = newTasks;
  }
  
  @Action
  public submitTask(task: Task){  
    if(task.id){ // update
      this.context.commit('update', task);
    }else{ // add new
      const now = new Date();
      task.id = now.getTime();
      this.context.commit('add', {...task});
    }
    this.context.dispatch('saveTasks');
  }
  @Action
  public deleteTask(id: number){
    this.context.commit('delete', id);
    this.context.dispatch('saveTasks');
  }
  @Action
  public bulkDelete(ids: Array<string>){
    const idsNumber = ids.map(id => parseInt(id));
    this.context.commit('bulk', idsNumber);
    this.context.dispatch('saveTasks');
  }
  @Action
  public checkStorage(){
    let data = localStorageService.check(TASKS_KEY);
    if(data === null) data = [];
    this.context.commit('setState', data);
  }
  @Action
  public saveTasks(){
    localStorageService.save(TASKS_KEY, this.tasks);
  }
}
export default TaskModule;