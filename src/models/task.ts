export class Task{
  id!: number;
  title: string;
  description: string;
  dueDate: string;
  priority: string;
  constructor(id: number,title: string, description: string, dueDate: string, priority: string){
    this.id = id;
    this.title = title;
    this.description = description;
    this.dueDate = dueDate;
    this.priority = priority;
  }
}